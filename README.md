# Cypress Test Display

### A react project to display the structure of Cypress tests

In a 4 tier structure of
```$xslt
  Spec file
    Test group (cypress 'describe')
      Test (cypress'it')
        Comments ('cy.log')
```

A related Python file `automation_tests/cypress-tests/cypress-doc.py` 
creates the necessary data.   
See `data/cypress-data.json` here for an example structure.
Being React, this structure must be used exactly.


### Starting
```
$> yarn start
```
uses `package.json`

##Author
Simon Hewitt   
07799381001    
February 2019   

