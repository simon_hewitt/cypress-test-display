import React, {Component} from 'react';
// import logo from './logo.svg';
import './App.css';
import axios from 'axios';


class App extends Component {
    constructor(props) {
        super(props)
        this.state = data
    }

    componentWillMount() {
        const data_url = process.env.PUBLIC_URL + '/data/cypress-data.json'
        axios.get(data_url).then(res => {
            this.setState(res.data)
        }).catch(err => {
            alert('Axios get failed:' + err)
        })
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    Cypress Test documentation
                </header>
                <CypressFiles file_list={this.state.data}/>
            </div>
        );
    }
}

const CypressFiles = ({file_list}) =>
        <div className="cypress-file">
            {file_list.map((file_group) =>
                <div key={file_group.file_name} className="f2">
                    <h1> Spec file: {file_group.file_name} </h1>
                    <TestGroup test_groups={file_group.test_groups}/>
                </div>)
            }
        </div>


const TestGroup = ({test_groups}) =>
        <div className="test-group">
            {test_groups.map((test_group) =>
                <div key={test_group.test_group_name} className="test-group">
                    <h2> Test group : {test_group.test_group_name} </h2>
                    <Tests test_list={test_group.tests}/>
                </div>)
            }
        </div>

const  Tests =({test_list}) =>
    <div className="tests">
        {test_list.map((test) =>
            <div key={test.test} className="f2">
                <h3> Test : {test.test} </h3>

                <div className="test-comments">
                    {test.comments.map((comment, i) =>
                        <div key={i} className="f2">
                            <span> Comment: {comment} </span>
                        </div>)
                    }
                </div>
            </div>)}
    </div>



const data = {
    "data": [
        {
            "file_name": "Not Ready Yet",
            "test_groups": [
                {
                    "test_group_name": "Test group f1 g1",
                    "tests": [
                        {
                            "test": "IT test 1",
                            "comments": [
                                "comment 1-1",
                                "comment 1-2"
                            ]
                        },
                        {
                            "test": "IT test 2",
                            "comments": [
                                "comment 2-1",
                                "comment 2-2"
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}


export default App;
